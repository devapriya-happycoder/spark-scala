package com.happycoding

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._
import scala.io.Source
import java.nio.charset.CodingErrorAction
import scala.io.Codec

object PopularMovie { 
  
  def broadcastMovieNames () : Map[Int, String] = {
    
     // Handle character encoding issues:
    implicit val codec = Codec("UTF-8")
    codec.onMalformedInput(CodingErrorAction.REPLACE)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)
    
    var movienames:Map[Int,String] = Map()
    
    val lines = Source.fromFile("D:\\workspace\\SparkScala\\resources\\u.item").getLines()
    for(line <- lines){
      val input = line.split('|')
      movienames += (input(0).toInt -> input(1))
    }
    
    return movienames
  }
  
  def main(arsgs : Array[String]){
    
    Logger.getLogger("org").setLevel(Level.INFO) 
    
   
     val sc = new SparkContext("local[*]","PopularMovieusingBroadcast")
    
    var  movienames = sc.broadcast(broadcastMovieNames)
    
    val input = sc.textFile("D:\\workspace\\SparkScala\\resources\\u.data")
    
    val mappedinput = input.map( x => (x.split("\t")(1).toInt, 1))
    
    val reducedInput = mappedinput.reduceByKey( (x,y) => x+y)
    
    val flipped = reducedInput.map( x => (x._2,x._1))
    
    val sortedMovies = flipped.sortByKey()
    
    val mappedMovieNames = sortedMovies.map( x  => (movienames.value(x._2), x._1) )
        
    val results =  mappedMovieNames.collect()
    
    results.foreach(println)
    
    
    
  }
}