package com.happycoding

import org.apache.log4j._
import org.apache.spark.SparkContext


// Find out the list of High rated apss in Google Play Store
/*           // Split on comma
(?=         // Followed by
   (?:      // Start a non-capture group
     [^"]*  // 0 or more non-quote characters
     "      // 1 quote
     [^"]*  // 0 or more non-quote characters
     "      // 1 quote
   )*       // 0 or more repetition of non-capture group (multiple of 2 quotes will be even)
   [^"]*    // Finally 0 or more non-quotes
   $        // Till the end  (This is necessary, else every comma will satisfy the condition)
)*/
object PopularAndroidApp {
  
  def extractData(line :String)={
  //  val replacedLine = line.replaceAll("\"", "\"")    
    val fields = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)")
    val rating = fields(2).toDouble
    if(rating.isNaN){
       (0.0,fields(0))
    }else{
      (fields(2).toDouble,fields(0))
    }

  }
  
  def main(args : Array[String]){
    Logger.getLogger("org").setLevel(Level.ERROR) 
  
  
   val sc = new SparkContext("local[*]","PopularAndroidApp")
   val appsData = sc.textFile("googleplaystore.csv");
   val mappedData = appsData.map( extractData)
   val filteredData = mappedData.filter(x => x._1 != 0.0)
   val sortedData = filteredData.sortBy(_._1, false, 1)
   println(sortedData.first())
   
   /*Output
    * (5.0,Hojiboy Tojiboyev Life Hacks)
    * */
  }
    
}