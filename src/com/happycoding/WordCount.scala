package com.happycoding;

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._

/** Compute the average number of friends by age in a social network. */
object WordCount {
  
  def main(args : Array[String]){
    
    val sc = new SparkContext("local[*]","WordCount");
    val inputrdd = sc.textFile("$INPUT_FILE_NAME");
    
    val splitrdd = inputrdd.flatMap(x => x.split(" "));
    
    println("Output count : " +splitrdd.count());
    
    
    
  }
 
}
  