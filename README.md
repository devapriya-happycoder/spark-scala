This Scala project contains scala code snippets on various use cases.

Prerequisites
1. Need JDK 1.8
2. Need winutils to be set as HADOOP_HOME in case of Windows OS.

1. Import the project using Scala IDE.
2. Run the scala programs as Scala Application with JAVA_HOME & HADOOP_HOME as environment variables.


This Scala snippet finds average number of friends by age.
1. FriendsByAge.scala 